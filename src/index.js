import $ from 'jquery';
import '../node_modules/materialize-css/dist/css/materialize.min.css';
import '../node_modules/materialize-css/dist/js/materialize.min';
import './css/index.css';

$(document).ready(() => {
  const colors = ['#bbdefb','#b2dfdb','#d7ccc8', '#c8e6c9', '#f0f4c3', '#ffecb3', '#b39ddb', '#ef9a9a'];                
  const rand = () => Math.floor(Math.random() * colors.length);           
  $('body').css("background-color", colors[rand()]);

  $.ajax({
    method: 'GET',
    url: 'https://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1&callback=',
    dataType: "json",
    '_' : Date.now(),
    cache: false,
    success: data => {
      const post = data.shift(); // The data is an array of posts. Grab the first one.
      $('#quote-box').fadeOut('slow', () => {
        $('#author').text('- ' + post.title);
        $('#text').html('<i class="fa fa-quote-left left"></i>' + post.content);
        $('#quote-box').fadeIn('slow');
      });
      $('body').css("background-color", colors[rand()]);
    }
  });

  $('#new-quote').on('click', e => {
    e.preventDefault();
    $.ajax( {
      method: 'GET',
      url: 'https://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1&callback=',
      dataType: "json",
      '_' : Date.now(),
      cache: false,
      success: data => {
        const post = data.shift(); // The data is an array of posts. Grab the first one.
        $('#quote-box').fadeOut('slow', () => {
          $('#author').text('- ' + post.title);
          $('#text').html('<i class="fa fa-quote-left left"></i>' + post.content);
          $('#quote-box').fadeIn('slow');
        });
        $('body').css("background-color", colors[rand()]);
      }
    });
  });
});